<?php
function techtop_scripts() {
	wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/css/font-awesome.css', array(), '4.3.0' );
	wp_enqueue_style( 'typo', get_stylesheet_directory_uri() . '/css/typo.css', array(), '' );
	wp_enqueue_script( 'main', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery' ));
}
add_action( 'wp_enqueue_scripts', 'techtop_scripts' );