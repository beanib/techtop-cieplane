jQuery(document).ready(function($){

	//Eliminar los creditos
	//$( 'div#credits a' ).remove( 'a' );
	$( 'div#credits' ).empty();

	// Icons for Social Network
	$( 'li.social-profiles-widget a[href*="twitter"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="twitter"]' ).append( '<i class="fa fa-twitter-square"></i>' );
	$( 'li.social-profiles-widget a[href*="facebook"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="facebook"]' ).append( '<i class="fa fa-facebook-square"></i>' );
	$( 'li.social-profiles-widget a[href*="google"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="google"]' ).append( '<i class="fa fa-google-plus-square"></i>' );
	$( 'li.social-profiles-widget a[href*="linkedin"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="linkedin"]' ).append( '<i class="fa fa-linkedin-square"></i>' );
	$( 'li.social-profiles-widget a[href*="rss"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="rss"]' ).append( '<i class="fa fa-rss-square"></i>' );
	$( 'li.social-profiles-widget a[href*="email"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="email"]' ).append( '<i class="fa fa-envelope-square"></i>' );
	$( 'li.social-profiles-widget a[href*="youtube"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="youtube"]' ).append( '<i class="fa fa-youtube-square"></i>' );
	$( 'li.social-profiles-widget a[href*="pinterest"] img' ).remove( 'img' );
	$( 'li.social-profiles-widget a[href*="pinterest"]' ).append( '<i class="fa fa-pinterest-square"></i>' );
	
});
 